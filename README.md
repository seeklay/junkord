# junkord
This project was originally developed by [seeklay](https://gitlab.com/seeklay/) in Oct 2021 and partially rewriten in Feb 2023 for readability. I don't know why is this created, but what if it may help somebody in discord voice api researches.

synchronous library for interact with discord gateway / discord voice gateway

## requirements
* websocket
* scapy          [start.py]
* pynacl         [start.py] 
* pyaudio        [start.py] 
* discord.py     [start.py]

## start.py description
this script is my attempt to receive audio from discord voice channels [unsuccessful attempt]
what is it doing?
1. Connect to discord gateway using junkord
2. Send identify to gateway
3. Receive READY and save session_id and user_id
4. Send voice state update with channel and guild IDs
5. Receive VOICE_SERVER_UPDATE and save endpoint info
6. Send UDP datagram to endpoint to find public IP and PORT
7. Send SELECT_PROTOCOL to voice gateway with public IP and PORT
8. Receive SESSION_DESCRIPTION and save secret key
9. Start receiving UDP datagrams with other channel parcipants audio
10. Decode and decrypt each packet
11. [opus decoding not working!]

## Why is this attempt unsucessfull
Script successfully starts receiving rtp datagrams with encrypted audio, but after decrypting, opus codec returns this error for each packet: `OpusError: corruptedstream`
I don't know what is the cause of this behavior. It could be a decryption error or a format error. I don't think it's decryption error because `discord.py`  uses the same library and successfully encrypts packets. Discord are very `тут ниче интересного не было`, 'cause they doesn't provided audio receiving api docs and hide it from developers make it private.

## Further fate
I don't think i will update this project 'til discord drops voice receiving docs for everyone.

## Related links
#### discord offical api docs
https://discord.com/developers/docs/topics/gateway
https://discord.com/developers/docs/topics/voice-connections
https://discord.com/developers/docs/resources/voice

#### discord.py voice sources
https://github.com/Rapptz/discord.py/blob/master/discord/opus.py
https://github.com/Rapptz/discord.py/blob/master/discord/voice_client.py

#### discord-unofficial-docs (not about voice but very interesting too)
https://luna.gitlab.io/discord-unofficial-docs/

## One more thing
idk how discord reads rtp packets, rtp packets don't have len field and can be readed from socket at a random offset. i beat this bug by using scapy for raw udp datagrams reading

ах да за качество кода сори я ведь тогда только начинал с py-thon ом
