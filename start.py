from junkord import Gateway, VoiceGateway
import os, time, socket, struct, io
from scapy.all import sniff, IP, UDP
import nacl.secret
from discord import opus
import pyaudio

TOKEN = os.environ["TOKEN"]

gateway = Gateway(debug=True)
gateway.connect()
gateway.identify(TOKEN)
while not gateway.READY:
    pass

voice_server = None

def vseu(d):
    global voice_server
    voice_server = d
    print("VSEU")
    print(d)

def vstu(d):
    print("VSTU")
    print(d)

gateway.voice_server_update_evt = vseu
gateway.voice_state_update_evt = vstu

state = {"guild_id": "YOUR GUILD ID HERE", "channel_id": "YOUR CHANNEL ID HERE", "self_mute": False, "self_deaf": False}
gateway.voice_state_update(state)

while not voice_server:
    pass

voice_gateway = VoiceGateway(debug=True)
voice_gateway.connect(f"wss://{voice_server['endpoint']}?v=6")
credentials = {
    'server_id': voice_server['guild_id'],
    'user_id': gateway.user_id,
    'session_id': gateway.session_id,
    'token': voice_server['token']        
}
voice_gateway.identify(credentials)
while not voice_gateway.READY:
    pass

sock = socket.socket(2, 2)
print(f"Remote peer: {voice_gateway.ip}:{voice_gateway.port}")
payload = b"\x01\x46" + struct.pack('!I', voice_gateway.ssrc) + b"\x00"*64
sock.sendto(payload, (voice_gateway.ip, voice_gateway.port))
r = io.BytesIO(sock.recvfrom(5000)[0])
assert r.read(4) == b"\x01\x00\x46\x01"
ip = ''.join(iter(lambda: r.read(1).decode('ascii'), '\x00'))
port = struct.unpack('!H', r.read()[-2:])[0]
print(f"IP: {ip}   PORT: {port}")

protocol = {
    'protocol': 'udp',
    'data': {
        'address': ip,
        'port': port,
        'mode': 'xsalsa20_poly1305'
    }
}
voice_gateway.select_protocol(protocol)

while not voice_gateway.SD:
    pass

key = bytes(voice_gateway.secret)
cipher = nacl.secret.SecretBox(key)
od = opus.Decoder()
p = pyaudio.PyAudio()
#s = p.open(output=True, rate=48000, channels=2, format=8)

while 1:
    try:
        p = sniff(filter=f"udp and port {sock.getsockname()[1]}", count=1)[0]
    except:
        break        
    b = io.BytesIO(bytes(p[3]))
    vf = b.read(1) # version + flags
    pt = b.read(1) # packet type
    if vf != b"\x90" or pt != b"\x78":
        continue

    seqr = b.read(2)
    seq = struct.unpack('!H', seqr) # seq num
    tsr = b.read(4)
    ts = struct.unpack('!I', tsr) # timestamp
    ssrcr = b.read(4)
    ssrc = struct.unpack('!I', ssrcr) # ssrc
    data = b.read()

    print(f"SEQ: {seq} ::: TS: {ts} ::: SSRC: {ssrc}")
    
    print('Len Before Decrypt:', len(data))
    nc = vf + pt + seqr + tsr + ssrcr + b"\x00"*12
    data = cipher.decrypt(data, nonce=nc)
    print('Len After Decrypt:', len(data))

gateway.stop()
voice_gateway.stop()
print("Exiting...")
