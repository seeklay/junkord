
from enum import Enum
import json

class DiscordVCPacket:
    
    op = None
    d = None

    @classmethod
    def deserialize(cls, packet):
        obj = cls() 
        pack = json.loads(packet)
        obj.op = DiscordVCpacketsByServer(pack['op'])
        obj.d = pack['d']
        return obj

    @staticmethod
    def serialize(op, d):
        pack = {
            "op": op,
            "d": d           
        }
        return json.dumps(pack)

class DiscordVCpacketsByServer(Enum):

    Ready = 2
    Session_Description = 4
    Speaking = 5
    Heartbeat_ACK = 6
    Hello = 8
    Resumed = 9
    Client_Disconnect = 13

    Webcam_on = 12 # unoffical
    UNDEF = 15
    UDEF = 14

class DiscordVCpacketsByClient(Enum):

    Identify = 0
    Select_Protocol = 1
    Heartbeat = 3
    Speaking = 5
    Resume = 7