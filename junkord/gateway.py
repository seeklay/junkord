from .jnlog2 import jnlog
import websocket
import time
from threading import Thread
from .packs import *

class Gateway:
    
    WS_OPENED: bool = False
    WS_THREAD: Thread
    READY:bool = False
    WS_OPEN_TIME: int

    def __init__(self, debug=True):
        self.logger = jnlog(name='JunkordGW', loglevel=1)
    
    def connect(self):
        url = "wss://gateway.discord.gg/?v=9&encoding=json"
        self.logger.info(f'Attempting to connect to {url}')        
        self.WebSocket = websocket.WebSocketApp(url,
            on_open=self._ws_opn,
            on_message=self._ws_msg,
            on_error=self._ws_err,
            on_close=self._ws_cls
        )
        self.WS_THREAD = Thread(target=self.WebSocket.run_forever, daemon=True)
        self.WS_THREAD.start()
        self.WS_OPEN_TIME = time.time()
        while not self.WS_OPENED:
            pass
        
    def _ws_opn(self, o):
        self.WS_OPENED = True
        self.logger.info(f"WebSocket connection opened in {round((time.time() - self.WS_OPEN_TIME) * 1000, 2)}ms")
    
    def _ws_msg(self, c, message):
        p = DiscordGWPacket.deserialize(message)
        self.SEQ = p.s
        m = getattr(self, f'_int_{p.op.name}') if hasattr(self, f'_int_{p.op.name}') else None

        if m:
            if p.op.name == "Dispatch":
                m(p.d, p.t)
            else:
                m(p.d)
        else:
            self.logger.warn(f"Unhandled {p.op.name}")
    
    def _ws_err(self, c, e):
        self.logger.warn(f"[WS-ERR] {c}, {e}")
    
    def _ws_cls(self, c, m, d):
        self.logger.warn(f"[WS-CLOSE] {m}, {d}")
    
    def stop(self):
        self.WebSocket.close()        

    def heartbeat(self, interval: int):
        t = time.time()
        while 7:
            if (time.time() - t) * 1000 >= interval:
                t = time.time()
                self.WebSocket.send(DiscordGWPacket.serialize(1, self.SEQ))
                self.logger.info(f"[HEARTBEAT] sent")
            time.sleep(0.17)

    def _int_Hello(self, d):
        self.logger.info(f"[HELLO] {d}")
        self.HB_THREAD = Thread(target=self.heartbeat, daemon=True, args=(int(d['heartbeat_interval']),))
        self.HB_THREAD.start()
        self.logger.info(f"[HELLO] Heartbeat Interval: {d['heartbeat_interval']}")    
    
    def _int_Heartbeat_ACK(self, d):
        self.logger.info(f"[HEARTBEAT] ack {d}")
    
    def _int_Dispatch(self, d, t):
        self.logger.info(f"[DISP] {d} {t}")
        if t == 'READY':
            self.session_id = d['session_id']
            self.user_id = d['user']['id']
            self.READY = True
        elif t == 'VOICE_STATE_UPDATE':
            self.voice_state_update_evt(d) if hasattr(self, 'voice_state_update_evt') else None
        
        elif t == 'VOICE_SERVER_UPDATE':
            self.voice_server_update_evt(d) if hasattr(self, 'voice_server_update_evt') else None
    
    def identify(self, token):
        d = {
            "token": token,
            #"capabilities": 125,
            "intents": 212,
            "properties": {
                "os": 'x86 comp',
                "browser": 'Junkord',
                'device': 'library'
            }
        }
        self.WebSocket.send(DiscordGWPacket.serialize(2, d))
        self.logger.info("sent idntfy")
    
    def voice_state_update(self, state):
        self.WebSocket.send(DiscordGWPacket.serialize(4, state)) 