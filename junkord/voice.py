from .jnlog2 import jnlog
import websocket
import time
from threading import Thread
from .vpacks import *

class VoiceGateway:

    WS_OPENED: bool = False
    WS_THREAD: Thread
    READY:bool = False
    WS_OPEN_TIME: int
    SD: bool = False

    def __init__(self, debug=True):        
        self.logger = jnlog(name='JunkordVoice', loglevel=1)

    def connect(self, endpoint):
        self.logger.info(f'Attempting to connect to {endpoint}')        
        self.WebSocket = websocket.WebSocketApp(endpoint,
            on_open=self._ws_opn,
            on_message=self._ws_msg,
            on_error=self._ws_err,
            on_close=self._ws_cls
        )
        self.WS_THREAD = Thread(target=self.WebSocket.run_forever, daemon=True)
        self.WS_THREAD.start()
        self.WS_OPEN_TIME = time.time()
        while not self.WS_OPENED:
            pass

    def _ws_opn(self, o):
        self.WS_OPENED = True
        self.logger.info(f"WebSocket connection opened in {round((time.time() - self.WS_OPEN_TIME) * 1000, 2)}ms")
    
    def _ws_msg(self, c, message):
        p = DiscordVCPacket.deserialize(message)
        m = getattr(self, f'_int_{p.op.name}') if hasattr(self, f'_int_{p.op.name}') else None
        print(p.op, p.d)
        if m:
            m(p.d)
    
    def _ws_err(self, c, e):
        self.logger.warn(f"[VO-WS-ERR] {c}, {e}")
    
    def _ws_cls(self, c, m, d):
        self.logger.warn(f"[VO-WS-CLOSE] {m}, {d}")
    
    def stop(self):
        self.WebSocket.close()        
    
    def heartbeat(self, interval: int):
        t = time.time()
        while 7:
            if (time.time() - t) * 1000 >= interval:
                t = time.time()
                self.WebSocket.send(DiscordVCPacket.serialize(DiscordVCpacketsByClient.Heartbeat.value, 751573733))
                self.logger.info(f"[VO-HEARTBEAT] sent")
            time.sleep(0.17)

    def _int_Hello(self, d):
        self.logger.info(f'[VO-HELLO] Received Hello Message. Heartbeat interval: [{d["heartbeat_interval"]}]')
        self.HB_THREAD = Thread(target=self.heartbeat, daemon=True, args=(int(d['heartbeat_interval']),))
        self.HB_THREAD.start()
    
    def identify(self, d):
        self.WebSocket.send(DiscordVCPacket.serialize(DiscordVCpacketsByClient.Identify.value, d))
    
    def _int_Ready(self, d):
        self.logger.info(f"[READY] ::: {d}")        
        self.ssrc = d["ssrc"]
        self.ip = d["ip"]
        self.port = d["port"]
        self.READY = True
    
    def select_protocol(self, d):        
        self.WebSocket.send(DiscordVCPacket.serialize(DiscordVCpacketsByClient.Select_Protocol.value, d))
    
    def _int_Session_Description(self, d):
        self.secret = d["secret_key"]
        self.SD = True