from enum import Enum
import json

class DiscordGWPacket:
    
    op = None
    d = None
    s = None
    t = None

    @classmethod
    def deserialize(cls, packet):

        obj = cls()
        pack = json.loads(packet)
        obj.op = DiscordGWpacketsByServer(pack['op'])
        obj.d = pack['d']
        obj.s = pack['s']
        obj.t = pack['t']
        return obj

    @staticmethod
    def serialize(op, d):
        pack = {
            "op": op,
            "d": d           
        }
        return json.dumps(pack)
    
    def __str__(self):
        return f"OP: {self.op}  S: {self.s}  T: {self.t}  D: {self.d}"

class DiscordGWpacketsByServer(Enum):

    Dispatch = 0
    Heartbeat = 1 
    Reconnect = 7
    Invalid_session = 9
    Hello = 10
    Heartbeat_ACK = 11

class DiscordGWpacketsByClient(Enum):

    Heartbeat = 1 
    Identidy = 2
    Presence_update = 3
    Voice_state_update = 4
    Resume = 6
    Req_guild_members = 8