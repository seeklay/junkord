import datetime
import enum
import os

class jnlog:
    def __init__(self, name, loglevel=2):
        """
        log fmt : '[hh/mm/ss] [level] [name] [message]'        
        """
        self.loglevel = loglevel
        self.name = name
        os.system('color')

    def __log(self, color, type, a):
        """
        internal log backend
        """
        t = datetime.datetime.now().time()

        hour = str(t.hour)
        min = str(t.minute)
        if len(min) == 1:
            min = '0'+min
        sec = str(t.second)
        if len(sec) == 1:
            sec = '0'+sec
        
        txt = ''
        for x in a:
            txt += f"{x} "

        print(f'{color} [{hour}/{min}/{sec}] [{type}] [{self.name}] {txt} \033[0m')        

    def info(self, *a):
        """
        simple log fn [INFO]
        loglevel: ALL
        """
        if self.loglevel != 1:
            return
        
        r, g, b = 72, 219, 251
        color = f'\x1b[38;2;{r};{g};{b}m'
        color = '\033[96m'
        self.__log(color, 'INFO', a)
    
    def warn(self, *a):
        """
        warning log fn [WARN]
        loglevel: WARN
        """
        if self.loglevel != 2 and self.loglevel != 1:
            return
        
        r, g, b = 255, 242, 0
        color = f'\x1b[38;2;{r};{g};{b}m'        
        color = '\033[93m'
        self.__log(color, 'WARN', a)
    
    def severe(self, *a):
        """
        severe log fn [SEVERE]
        """
        r, g, b = 179, 57, 57
        color = f'\x1b[38;2;{r};{g};{b}m'
        color = '\033[91m'
        self.__log(color, 'SEVERE', a)
        exit()


        